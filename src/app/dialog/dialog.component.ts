import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  configForm: FormGroup;
  weaponTypes: Array<any>;
  weaponOptics: Array<any>;
  weaponAmmo: Array<any>;
  configModel: ConfigModel;

  constructor(private fb: FormBuilder, private http: HttpClient) {
  }

  ngOnInit(): void {
    const bisUrl = 'http://67.205.147.92:8080/api/v1/';
    this.http.get(`${bisUrl}weapons`).subscribe((res: Array<object>) => this.weaponTypes = res);
    this.http.get(`${bisUrl}ammunition`).subscribe((res: Array<object>) => this.weaponAmmo = res);
    this.http.get(`${bisUrl}attachments`).subscribe((res: Array<object>) => this.weaponOptics = res);
    this.initForm();
    this.configForm.valueChanges.subscribe(res => {
      this.configModel = res;
      localStorage.setItem('savedConfig', JSON.stringify(this.configModel));
    });
  }

  initForm(): void {
    const model: ConfigModel = {
      Name: 'New Configuration',
      Type: {},
      Ammo: {},
      Optic: {},
    };
    const storedConfig = !localStorage.getItem('savedConfig') ? model : JSON.parse(localStorage.getItem('savedConfig'));
    this.configForm = this.fb.group({
      'Name': new FormControl(storedConfig.Name, []),
      'Optic': new FormControl(storedConfig.Optic, []),
      'Type': new FormControl(storedConfig.Type, []),
      'Ammo': new FormControl(storedConfig.Ammo, []),
    });
    this.configModel = this.configForm.value;
  }

  resetForm(): void {
    this.configForm.reset();
  }

  submitConfig(value: object): void {
    console.log('Submitted Successfully', value);
  }

}

export interface ConfigModel {
  Name: string;
  Type: object | any;
  Ammo: object | any;
  Optic: object | any;
}
